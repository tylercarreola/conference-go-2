import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_picture(city):
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(
        f"https://api.pexels.com/v1/search?query={city}", headers=headers
    )
    response = response.json()

    return response["photos"][0]["src"]["original"]


def get_weather(location):
    geo_location_api = f"http://api.openweathermap.org/geo/1.0/direct?q={location.city.title()},US-{location.state},US&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(geo_location_api).json()[0]


    if response:
        location = {
            "lat": response["lat"],
            "lon": response["lon"]
        }

        temp_location_api = f"https://api.openweathermap.org/data/3.0/onecall?lat={location['lat']}&lon={location['lon']}&exclude=minutely,hourly,daily,alerts&units=imperial&appid={OPEN_WEATHER_API_KEY}"
        response = requests.get(temp_location_api).json()["current"]
        response = {"temp": response["temp"], "description": response["weather"][0]["description"]}

    else:
        return "Weather information not found"

    return response
